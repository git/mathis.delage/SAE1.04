# SAE1.04

Cette SAE à été réalisé par _Mathieu GROUSSEAU_ _Cléo EIRAS_ _Mathis MOULIN_ et _Mathis DELAGE_

Elle se compose de plusieurs fichiers:

1. location.sql qui contient:

    > Notre base de données.  
    > Les requètes SQL demanandées.

2. Question Economie.pdf:

    > Réponses aux question d'éconi=omie sur notre entreprise.

3. Presentation.pdf:

    > Présentation de notre entreprise et de notre base de données.

4. MCD.png:

    > Shéma de notre Modèle Conceptuelle de Données.

5. MLD.png:

    > Shéma de notre Modèle Logique de Données.
