DROP TABLE Location;
DROP TABLE Disposer;
DROP TABLE Vehicule;
DROP TABLE Client;
DROP TABLE Fournisseur;    
DROP TABLE Modele;
DROP TABLE Option;

CREATE TABLE Client(
    id_client numeric(6) PRIMARY KEY,
    type char(1) NOT NULL,
    nom varchar(15) NOT NULL,
    prenom varchar(15) NOT NULL,
    date_naiss date,
    permis varchar(3) NOT NULL,
    tel numeric(10) NOT NULL,
    code_postal numeric(5) NOT NULL,
    ville varchar(50) NOT NULL,
    rue varchar(50) NOT NULL,
    CONSTRAINT type_client CHECK (type IN ('E', 'P')),
    CONSTRAINT date_naiss CHECK (CURRENT_DATE::date - date_naiss > (18 * 365)),
    CONSTRAINT permis_check 
    CHECK (permis IN ('AM', 'A1', 'A2', 'A', 'B1', 'B', 'C1', 'C', 'D1', 'D', 'BE', 'C1E', 'CE', 'D1E', 'DE'))
);

CREATE TABLE Option(
    nom varchar(30) PRIMARY KEY,
    description varchar,
    prix_supp numeric(6, 2) NOT NULL,
    CHECK(prix_supp >= 0)
);

CREATE TABLE Fournisseur(
    id numeric(6) PRIMARY KEY,
    nom varchar(30) NOT NULL,
    type char(1) NOT NULL,
    code_postal numeric(5) NOT NULL,
    ville varchar(50) NOT NULL,
    rue varchar(50) NOT NULL,

    CONSTRAINT type_ok CHECK (type IN ('P', 'E'))
);

CREATE TABLE Modele(
    marque varchar,
    modele varchar,
    prix_base numeric(11, 2) NOT NULL,
    permis varchar(3) NOT NULL,

    PRIMARY KEY (marque, modele),

    CONSTRAINT permis_check 
    CHECK (permis IN ('AM', 'A1', 'A2', 'A', 'B1', 'B', 'C1', 'C', 'D1', 'D', 'BE', 'C1E', 'CE', 'D1E', 'DE'))
);

CREATE TABLE Vehicule(
    immatriculation char(9) PRIMARY KEY,
    id_fournisseur numeric(6) NOT NULL REFERENCES Fournisseur,
    marque varchar,
    modele varchar,
    km numeric(10, 1) NOT NULL,
    acquisition date NOT NULL,

    FOREIGN KEY (marque, modele) REFERENCES Modele,
    CONSTRAINT immat CHECK(immatriculation LIKE '__-___-__'),
    CHECK(km >= 0)
);

CREATE TABLE Location (
    id_location numeric(5) PRIMARY KEY,
    id_client numeric(5) NOT NULL REFERENCES Client,
    immatriculation char(9) NOT NULL REFERENCES Vehicule,
    date_debut date NOT NULL,
    date_fin date NOT NULL,
    caution numeric(10,2) NOT NULL,
    montant numeric(10,2) NOT NULL,

    CONSTRAINT montant_ok CHECK (montant >= 0),
    CONSTRAINT caution_ok CHECK (caution >= 0),
CONSTRAINT dates_ok CHECK (date_debut < date_fin)
);

CREATE TABLE Disposer (
    immatriculation char(9) REFERENCES Vehicule,
    nom_option varchar(30) REFERENCES Option,
    PRIMARY KEY (immatriculation, nom_option)
);



INSERT INTO Client 
VALUES (0, 'P', 'Le Duc', 'Jean', '15/05/1995', 'A', 0610792243, 43000, 'Le Puy en Velay', '10 Rue Voltaire');
INSERT INTO Client
VALUES (1, 'E', 'Diesel', 'Vincent', '22/01/1930', 'C1E', 0410692243, 73000, 'Clermont Ferrand', '21 Rue Claude Monnnet');
INSERT INTO Client
VALUES (2, 'P', 'Perriand', 'Charlotte', '24/10/1903', 'D', 0746563543, 75056, 'Paris', '7e arrondissement');

INSERT INTO Option VALUES ('Siège chauffant', 'Siège chauffant en cuir conducteur et passagers', 100.0);
INSERT INTO Option VALUES ('Toit ouvrant', NULL, 50.0);
INSERT INTO Option VALUES ('Ligne inox', 'Ligne d''échappement en inox', 250.0);
INSERT INTO Option VALUES ('GPS', 'Assistant GPS intégré au tableau de bord', 50.5);

INSERT INTO Fournisseur VALUES (0, 'Citronen', 'E', 75056, 'Paris', '15 Rue des Meuniers');
INSERT INTO Fournisseur VALUES (1, 'Syndie Rose', 'P', 69160, 'Amiens', '32 Rue des Pâquerettes');

INSERT INTO Modele VALUES ('Citroen', 'Saxo', 100.25, 'B');
INSERT INTO Modele VALUES ('Ferrari', '296 GTS', 2000.99, 'B');
INSERT INTO Modele VALUES ('Delage', 'D12', 3000.99, 'B');
INSERT INTO Modele VALUES ('DeLorean', 'DMC12', 1000.0, 'B');
INSERT INTO Modele VALUES ('Suzuki', 'V-STORM 800DE', 150.75, 'A');

INSERT INTO Vehicule VALUES ('FR-302-WQ', 0, 'Citroen', 'Saxo', 10000.6, '16/12/2004');
INSERT INTO Vehicule VALUES ('FR-666-DE', 1, 'Ferrari', '296 GTS', 75000.0, '21/02/2011');
INSERT INTO Vehicule VALUES ('FR-667-ZU', 1, 'Delage', 'D12', 1000000, '21/02/2011');
INSERT INTO Vehicule VALUES ('AS-719-JV', 1, 'DeLorean', 'DMC12', 1500000, '19/10/2012');
INSERT INTO Vehicule VALUES ('IA-670-HE', 1, 'Suzuki', 'V-STORM 800DE', 9500, '07/05/2015');
INSERT INTO Vehicule VALUES ('BW-943-AY', 0, 'Citroen', 'Saxo', 7892.25, '29/04/2016');

INSERT INTO Location VALUES (0, 2, 'FR-667-ZU', '04/12/2022', '05/12/2022', 10000.0, 3000.99);
INSERT INTO Location VALUES (1, 0, 'IA-670-HE', '15/08/2022', '15/09/2022', 5000.2, 1500.30);
INSERT INTO Location VALUES (2, 0, 'BW-943-AY', '15/08/2022', '30/10/2022', 7500.5, 1200.00);
INSERT INTO Location VALUES (4, 2, 'FR-302-WQ', '11/12/2022', '25/12/2022', 200.6, 1000.60);
INSERT INTO Location VALUES (5, 1, 'FR-666-DE', '09/12/2022', '12/12/2022', 3120.0, 5600.00);
INSERT INTO Location VALUES (7, 2, 'IA-670-HE', '13/04/2022', '05/05/2022', 1500.6, 3000.22);
INSERT INTO Location VALUES (8, 0, 'FR-302-WQ', '06/06/2020', '27/08/2020', 3000.8, 6420.10);
INSERT INTO Location VALUES (9, 2, 'BW-943-AY', '14/03/2021', '14/03/2022', 5000.0, 20000.00);
INSERT INTO Location VALUES (10, 0, 'FR-666-DE', '24/01/2021', '25/01/2021', 30.8, 100.00);
INSERT INTO Location VALUES (11, 0, 'FR-667-ZU', '03/08/2021', '21/10/2021', 1300.3, 2000.15);
INSERT INTO Location VALUES (12, 1, 'BW-943-AY', '02/04/2022', '05/06/2022', 320.0, 3800.08);
INSERT INTO Location VALUES (13, 2, 'BW-943-AY', '07/11/2022', '20/11/2022', 400.0, 2600.32);
INSERT INTO Location VALUES (14, 1, 'IA-670-HE', '08/03/2021', '08/05/2021', 1200.2, 5230.00);
INSERT INTO Location VALUES (16, 1, 'FR-302-WQ', '17/10/2020', '05/01/2021', 5000.0, 20000.99);
INSERT INTO Location VALUES (17, 1, 'FR-667-ZU', '21/03/2022', '06/04/2022', 1000.0, 3800.00);
INSERT INTO Location VALUES (18, 0, 'FR-302-WQ', '23/04/2021', '06/07/2021', 690.5, 2430.50);
INSERT INTO Location VALUES (19, 2, 'FR-666-DE', '02/08/2022', '21/09/2022', 2345.2, 6520.90);

INSERT INTO Disposer VALUES ('FR-302-WQ', 'Toit ouvrant');
INSERT INTO Disposer VALUES ('FR-302-WQ', 'Siège chauffant');
INSERT INTO Disposer VALUES ('FR-666-DE', 'Ligne inox');
INSERT INTO Disposer VALUES ('AS-719-JV', 'GPS');

/*
SELECT * FROM Option;
SELECT * FROM Modele;
SELECT * FROM Fournisseur;
SELECT * FROM Client;
SELECT * FROM Disposer;
SELECT * FROM Location;
SELECT * FROM Vehicule;
*/

-- Question 1
SELECT DISTINCT m.marque, m.modele
FROM Vehicule v, Modele m
WHERE v.marque = m.marque
AND v.modele = m.modele
AND v.immatriculation NOT IN (SELECT immatriculation FROM Location);

/*
  marque  | modele
----------+--------
 DeLorean | DMC12
(1 ligne)
*/

-- Question 2
SELECT (
    SELECT SUM(Location.montant)
    FROM Client, Location
    WHERE
   	 Client.id_client = Location.id_client AND
   	 Client.type = 'E' AND
   	 CURRENT_DATE - Location.date_debut > 7
) AS AVANT, (
    SELECT SUM(Location.montant)
    FROM Client, Location
    WHERE
   	 Client.id_client = Location.id_client AND
   	 Client.type = 'E' AND
   	 CURRENT_DATE - Location.date_debut <= 7
) AS APRES;

/*
  avant   |  apres
----------+---------
 32831.07 | 5600.00
(1 ligne)
*/

-- Question 3
SELECT V.immatriculation, M.marque, M.modele                                                                                                                                                 	FROM VEHICULE V, MODELE M
WHERE V.marque=M.marque
AND V.modele=M.modele
AND V.immatriculation NOT IN (SELECT immatriculation FROM Location);

/*
 immatriculation |  marque  | modele
-----------------+----------+--------
 AS-719-JV   	| DeLorean | DMC12
(1 ligne)
*/

-- Question 4
SELECT (
    SELECT SUM(Location.montant)
    FROM Client, Location
    WHERE
   	 Client.id_client = Location.id_client AND
   	 Client.type = 'E' AND
   	 date_trunc('month', Location.date_debut) = date_trunc('month', CURRENT_DATE)
) AS entreprise, (
    SELECT SUM(Location.montant)
    FROM Client, Location
    WHERE
   	 Client.id_client = Location.id_client AND
   	 Client.type = 'P' AND
   	 date_trunc('month', Location.date_debut) = date_trunc('month', CURRENT_DATE)
) AS particulier;

/*
 entreprise | particulier
------------+-------------
	5600.00 | 	4001.59
(1 ligne)
*/
